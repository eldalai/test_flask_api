import unittest

from app import app


class BaseTestClass(unittest.TestCase):

    def setUp(self):
        self.app = app
        self.client = self.app.test_client()

    def tearDown(self):
        pass


class BlogClientTestCase(BaseTestClass):

    def test_index_with_no_posts(self):
        res = self.client.get('/hello')
        self.assertEqual(200, res.status_code)
        self.assertEqual({'hello': 'world'}, res.json)


if __name__ == '__main__':
    unittest.main()
